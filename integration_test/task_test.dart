import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:task_list/main.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Добавление задачи в приложении', (tester) async {
    const testKey = Key('K');
    const test1Key = Key('L');
    await tester.pumpWidget(const MyApp());
    await tester.tap(find.byKey(test1Key));
    await tester.pumpAndSettle();

    await tester.enterText(find.byType(TextField), 'Первая задача');
    await tester.tap(find.byKey(testKey));

    expect(find.text('Первая задача'), findsOneWidget);
    await tester.pumpAndSettle();

    await tester.tap(find.text('Добавить задачу'));
    await tester.pumpAndSettle();

    await tester.enterText(find.byType(TextField), 'Вторая задача');
    await tester.tap(find.text('Добавить'));

    expect(find.text('Вторая задача'), findsOneWidget);
  });
}
