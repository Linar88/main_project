import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class Todo with ChangeNotifier {
  final todos = <String>[];

  void addTodo(String text) {
    todos.add(text);
    notifyListeners();
  }
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static const _appTitle = 'Список задач';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => Todo(),
      child: MaterialApp(
        title: _appTitle,
        home: Scaffold(
          appBar: AppBar(
            title: const Text(_appTitle),
          ),
          body: const TodoList(),
        ),
      ),
    );
  }
}

class TodoList extends StatefulWidget {
  const TodoList({super.key});

  @override
  State<TodoList> createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  static const test1Key = Key('L');
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Expanded(
            child: Consumer<Todo>(
              builder: (context, todo, child) => ListView.builder(
                itemCount: todo.todos.length,
                itemBuilder: (context, index) {
                  final t = todo.todos[index];
                  return ListTile(title: Text(t));
                },
              ),
            ),
          ),
          ElevatedButton(
            key: test1Key,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const NewTask()),
              );
            },
            child: const Text('Добавить задачу'),
          ),
        ],
      ),
    );
  }
}

class NewTask extends StatefulWidget {
  const NewTask({super.key});
  @override
  State<NewTask> createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTask> {
  final controller = TextEditingController();
  static const testKey = Key('K');

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_MyAppState._appTitle),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Введите текст новой задачи',
              ),
              controller: controller,
              autofocus: true,
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  ElevatedButton(
                    key: testKey,
                    onPressed: () {
                      var todo = context.read<Todo>();
                      todo.addTodo(controller.text);
                      Navigator.pop(context);
                    },
                    child: const Text('Добавить'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Отмена'),
                  ),
                ]),
          ],
        ),
      ),
    );
  }
}
