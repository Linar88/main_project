import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:task_list/main.dart';

void main() {
  testWidgets('Добавить задачи и просмотр и список задач', (tester) async {
    const testKey = Key('K');
    const test1Key = Key('L');
    await tester.pumpWidget(const MyApp());
    await tester.tap(find.byKey(test1Key));
    await tester.pumpAndSettle();

    await tester.enterText(find.byType(TextField), 'Первая задача');
    await tester.tap(find.byKey(testKey));

    expect(find.text('Первая задача'), findsOneWidget);
    await tester.pumpAndSettle();

    await tester.tap(find.text('Добавить задачу'));
    await tester.pumpAndSettle();

    await tester.enterText(find.byType(TextField), 'Вторая задача');
    await tester.tap(find.text('Добавить'));

    expect(find.text('Вторая задача'), findsOneWidget);
  });
}
